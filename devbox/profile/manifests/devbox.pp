class profile::devbox {

    # override the default 5 min timeout of Puppet's Exec type
    Exec { timeout => 1800 }

    $chocolateyPackageList = [
	    'powershell4',
    	'DotNet4.5',
    	'git.install',
    	'resharper',
    	'git-credential-winstore',
    	'GoogleChrome',
    	'sublimetext3',
        'SublimeText3.PackageControl',
    	'poshgit',
    	'console-devel',
    	'p4merge',
    	'nunit',
    	'psget',
    	'7zip',
    	'Jump-Location',
    	'chromedriver2',
        'Cmder',
        'Gow',
        'Sysinternals',
        'gitextensions',
        'linqpad4'
    ]

    $winFeatures = [
		'AS-Named-Pipes',
		'AS-TCP-Activation',
		'AS-WAS-Support',
		'NET-Framework',
		'NET-Framework-45-ASPNET',
#		'NET-Framework-Core',
#		'NET-HTTP-Activation',
#		'NET-Non-HTTP-Activ',
		'NET-Win-CFAC',
		'RSAT-Web-Server',
		'Telnet-Client',
		'WAS',
		'WAS-Config-APIs',
#		'WAS-NET-Environment',
		'WAS-Process-Model',
		'Web-App-Dev',
#		'Web-Asp-Net',
		'Web-Asp-Net45',
		'Web-Basic-Auth',
		'Web-Client-Auth',
		'Web-Common-Http',
		'Web-Default-Doc',
		'Web-Dir-Browsing',
		'Web-Filtering',
		'Web-Health',
		'Web-Http-Errors',
		'Web-Http-Logging',
		'Web-Http-Redirect',
		'Web-Http-Tracing',
		'Web-ISAPI-Ext',
		'Web-ISAPI-Filter',
		'Web-Lgcy-Mgmt-Console',
		'Web-Lgcy-Scripting',
		'Web-Metabase',
		'Web-Mgmt-Compat',
		'Web-Mgmt-Console',
		'Web-Mgmt-Service',
		'Web-Mgmt-Tools',
#		'Web-Net-Ext',
		'Web-Performance',
		'Web-Request-Monitor',
		'Web-Security',
		'Web-Server',
		'Web-Stat-Compression',
		'Web-Static-Content',
		'Web-WebServer',
		'Web-Windows-Auth',
		'Web-WMI',
		'WinRM-IIS-Ext'
	]
    
    exec { 'Set-ExecutionPolicy':
	    command   => 'Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Force',
	    unless    => 'if (Get-ExecutionPolicy -eq "RemoteSigned") { exit 1 }',
	    provider  => powershell,
    }

    windowsfeature { $winFeatures:
		restart => true,
	} ->
    exec { 'Install-Chocolatey':
	    command   => 'iex ((new-object net.webclient).DownloadString("https://chocolatey.org/install.ps1"))',
	    unless    => 'if ((Get-command choco*) -ne $null) { exit 1 }',
	    provider  => powershell,
    } ->
    package { $chocolateyPackageList:
    	ensure => installed,
    	provider    => 'chocolatey',
    } ->
    exec { 'install_psreadline':
	    command   => 'Install-Module psreadline -addtoprofile',
	    provider  => powershell,
    }
     -> notify{'Now you need to intstall Visual Studio & SQL Server':}

 ###### - On server 2012 this package doesnt install successfully
 #	->
 # 	package { 'VisualStudio2013Ultimate':
 #    	ensure => installed,
 #    	install_options => ['-InstallArguments', '"/Features:\'SQL', 'WebTools\'"'],
 #    	# Refer here for install options handling: https://github.com/chocolatey/puppet-chocolatey/issues/15
 #    	#install_options => ['-InstallArguments', '"/Features:\'Blend','LightSwitch','VC_MFC_Libraries', 'OfficeDeveloperTools', 'SQL', 'WebTools', 'Win8SDK', 'SilverLight_Developer_Kit', 'WindowsPhone80\'', '/ProductKey:AAAA-AAAA-AAAA-AAAA-AAAA"'],
 #    	provider    => 'chocolatey',
 #    }

}