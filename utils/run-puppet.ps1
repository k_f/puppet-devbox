$ErrorActionPreference = "Stop"

# Temporaraly add puppet ruby to the path
$env:PATH += ";C:\Program Files (x86)\Puppet Labs\Puppet Enterprise\bin\"

#go to the repo root
$puppetDir = convert-path (get-item (get-item $PSCommandPath).PSParentPath).PSParentPath
push-location $puppetDir
& git pull

# install from the puppetFile
write-host Triggering librarian update
librarian-puppet update --verbose

write-host Triggering puppet run
write-host puppet apply --modulepath "$($puppetDir)\devbox;$($puppetDir)\modules" devbox\profile\tests\devbox.pp
puppet apply --modulepath "$($puppetDir)\devbox;$($puppetDir)\modules" devbox\profile\tests\devbox.pp