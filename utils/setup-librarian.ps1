$ErrorActionPreference = "Stop"

#go to the right folder for librarian-puppet
$puppetDir = (get-item (get-item $PSCommandPath).PSParentPath).PSParentPath

write-host running in $puppetDir

push-location $puppetDir

# Add puppet permanently to the path
if( -not $env:Path -contains "Puppet Enterprise\bin")
{
   write-host Adding Puppet Enterprise bin folder to path
   [Environment]::SetEnvironmentVariable("Path", $env:Path + ";C:\Program Files (x86)\Puppet Labs\Puppet Enterprise\bin", [EnvironmentVariableTarget]::Machine)
}
# Temporaraly add puppet ruby to the path
#$env:Path += ";C:\Program Files (x86)\Puppet Labs\Puppet Enterprise\sys\ruby\bin\"
$env:Path += ";C:\DevKit2\bin"
$env:Path += ";C:\tools\ruby200\bin\"

# Install chocolatey and then git
if( (get-command choco*) -eq $null)
{
    iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
}  else 
{
   write-host Detected that chocolatey is installed, will not reinstall
}
choco install git
# devkit is required to be able to install librarian-puppet dependancies
choco install ruby
RefreshEnv
choco install ruby2.devkit
RefreshEnv



# Install librarian-puppet & then trigger a Puppetfile install
write-host Installing puppet gem
gem install puppet
write-host Installing librarian-puppet gem
gem install librarian-puppet
write-host Running Puppetfile
librarian-puppet install --verbose

$ppPath = (join-path -path $puppetDir -childpath "utils\run-puppet.ps1")
write-host Triggering puppet run at $ppPath
. $ppPath

pop-location