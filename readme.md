# Dev Box Setup
## Prerequisites
- [Puppet Enterprise](http://puppetlabs.com/download-puppet-enterprise-thank-you-expand?ver=latest&dist=win)

## Setup instructions
- Clone the repository or download via zip
- Open a powershell console as **administrator**
- `cd` to the repo
- Run `.\utils\setup-librarian.ps1`

The setup script will do the following:

- Install chocolatey
- Install Git
- Install ruby (v2)
- Install ruby devtools
- Install the puppet librarian gem
- Add the puppet bin folder to your path
- Trigger a puppet run (see below)

#### Trouble Shooting the setup
- you may need to run the setup-librarian several times
- ruby seems to have problems with geting itself into the path and you may need to restart the command prompt to refresh
- some gems require win32 specific functionality but not grab them properly. If you see 

		... kernel_require.rb:55:in 'require': cannot load such file -- win32/process (LoadError) 
		
then you should  

		gem install win32-process
		gem install win32-dir
		gem install win32-services

- For some bizarre reason some VM's dont want to connect to the puppet forge properly because of cert errors. 

		... http.rb:920:in 'connect': SSL_connect returned=1 errno=0 state=SSLv3 read server certificate B: certificate verify failed (Faraday::SSLError)
		
then you should download the [GeoTrust Cert](https://www.geotrust.com/resources/root-certificates/) and then run the below in the same shell as your powershell scripts. See [http://railsapps.github.io/openssl-certificate-verify-failed.html](here) and [https://gist.github.com/fnichol/867550](here) for more info.
		
		set SSL_CERT_FILE=c:\temp\GeoTrust_Global_CA.pem

- If you get errors like ` No suitable tar implementation found` then you need to 
		
		gem install minitar


## Trigger puppet
run `utils\run-puppet.ps1` which:

- Triggers a puppet librarian install
- Triggers a puppet run against the devbox profile

